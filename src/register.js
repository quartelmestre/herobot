'use strict'

import { readdirSync, readFileSync } from 'node:fs'
import { join } from 'node:path'

import * as url from 'url'

import { REST } from '@discordjs/rest'
import { Routes } from 'discord-api-types/v9'

import { constants } from './library/_constants.js'

let auth

try {
    auth = JSON.parse(readFileSync('auth.json'))
    console.log('File auth.json has been read.')
} catch (err) {
    console.log(err)
}

// path for this file
const localPath = url.fileURLToPath(new URL('.', import.meta.url))

// commands is a an array to be populated by
// js source files in the commands directory
const commands = []

// join ensures that a path string conforms to local operating system format
const commandsPath = join(localPath, constants.COMMANDS_DIR)

// readdirSync reads all js files in commandsPath
const commandFiles = readdirSync(commandsPath).filter(file => file.endsWith('.js'))

// for each commands file:
for (const file of commandFiles) {
    // full name formed by path + filename
    const filePath = join(commandsPath, file)
    // dynamic import of source js file...
    import(filePath)
        .then(command => {
            // ... and its header is converted to JSON and included in
            // commands array
            commands.push(command.default.data.toJSON())
            console.log(`Loading command /${command.default.data.name}...`)
        })
        .catch(err => {
            console.log(`ERROR: ${err}`)
        })
}

// the bot token is read from auth.json file
const rest = new REST({ version: '9' }).setToken(auth.token)

if (typeof auth.guild !== 'undefined') {
    rest.put(Routes.applicationGuildCommands(auth.clientId, auth.guild), { body: commands })
        .then(() => {
            console.log('Successfully registered guild application commands.')
        })
        .catch(console.error)
} else {
    rest.put(Routes.applicationCommands(auth.clientId), { body: commands })
        .then(() => {
            console.log('Successfully registered global application commands.')
        })
        .catch(console.error)
}
