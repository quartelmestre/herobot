'use strict'

import 'dotenv/config.js'

import { readFileSync } from 'node:fs'
import { REST } from '@discordjs/rest'
import { Routes } from 'discord-api-types/v9'

let auth

try {
    auth = JSON.parse(readFileSync('auth.json'))
    console.log('File auth.json has been read.')
} catch (err) {
    console.log(err)
}

// the bot token is read from auth.json file
const rest = new REST({ version: '9' }).setToken(auth.token)

// we use Discord's API to get an array of all registered commands for this bot
// either as guild commands
// or as global commands
if (typeof auth.guild !== 'undefined') {
    rest.get(Routes.applicationGuildCommands(auth.clientId, auth.guild))
        .then(data => {
            const promises = []
            for (const command of data) {
                // each command is unregistered
                const deleteUrl = `${Routes.applicationGuildCommands(auth.clientId, auth.guild)}/${command.id}`
                promises.push(rest.delete(deleteUrl))
            }
            console.log('All previous guild commands have been unregistered.')
            return Promise.all(promises)
        })
        .catch(console.error)
} else {
    rest.get(Routes.applicationCommands(auth.clientId))
        .then(data => {
            const promises = []
            for (const command of data) {
                // each command is unregistered
                const deleteUrl = `${Routes.applicationCommands(auth.clientId)}/${command.id}`
                promises.push(rest.delete(deleteUrl))
            }
            console.log('All previous global commands have been unregistered.')
            return Promise.all(promises)
        })
        .catch(console.error)
}
