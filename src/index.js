'use strict'

import 'dotenv/config.js'

import { constants } from './library/_constants.js'

import { logError, logInfo, logDebug, logLevel } from './library/_logger.js'

import { GameState } from './library/_gamestate.js'

import { readdirSync, readFileSync } from 'node:fs'
import { join } from 'node:path'
import * as url from 'url'

import { Client, Collection, Intents } from 'discord.js'

// import * as auth from './auth.json' assert { type: 'json' }
let auth

try {
    auth = JSON.parse(readFileSync('auth.json'))
    logDebug('File auth.json has been read.')
} catch (err) {
    logError(err)
    console.log(err)
}

// path for this file
const localPath = url.fileURLToPath(new URL('.', import.meta.url))

const commandsPath = join(localPath, constants.COMMANDS_DIR)
const eventsPath = join(localPath, constants.EVENTS_DIR)

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] })

// this is a collection of gamestates
// the values are gamestate objects
// the indexes are guild ids
client.games = new Collection()

process.on('unhandledRejection', err => {
    logError(`General catch-up - unhandled promise rejection: ${err}`)
    console.log(err)
})

// Initialization procedure

// let's find the game files
const gamesPath = join(localPath, constants.GAMES_DIR)
const gameFiles = readdirSync(gamesPath).filter(file => file.endsWith('.json'))
for (const file of gameFiles) {
    const gs = new GameState()
    gs.readState(file)
    const previous = client.games.get(gs.guild)
    if (previous) {
        // if this guild already has a later gamestate loaded, break from the loop
        if (previous.time > gs.time) {
            logDebug(`Guild ${previous.guild} has a previous gamestate from ${previous.time} on `)
            break
        }
    }
    // we can now push the new gamestate to the games collection
    client.games.set(gs.guild, gs)
    logDebug(`Loaded file from ${gs.time} on guild ${gs.guild}`)
}


// Loads command files from the commands directory
client.commands = new Collection()
const commandFiles = readdirSync(commandsPath).filter(file => file.endsWith('.js'))
for (const file of commandFiles) {
    const filePath = join(commandsPath, file)
    const command = await import(filePath)
    // Set a new item in the Collection
    // with the key as the command name
    // and the value as the exported module
    client.commands.set(command.default.data.name, command)
}

// Loads event files
const eventFiles = readdirSync(eventsPath).filter(file => file.endsWith('.js'))
for (const file of eventFiles) {
    const filePath = join(eventsPath, file)
    const event = await import(filePath)
    // Sets a new event in the client
    if (event.once) {
        client.once(event.default.name, (...args) => event.default.execute(...args))
    } else {
        client.on(event.default.name, (...args) => event.default.execute(...args))
    }
}

client.on('ready', (c) => {
    // now we register the commands at Discord
    // if auth.json includes a guild
    if (typeof auth.guild !== 'undefined') {
        client.commands.forEach(command => {
            client.api.applications(auth.clientId).guilds(auth.guild).commands.post({
                data: {
                    name: command.default.data.name,
                    description: command.default.data.description,
                    options: command.default.data.options
                }
            })
        })
    }
    logInfo(`Herobot is ready! Logged in as ${c.user.tag}, log level is ${logLevel}`)
})

client.on('messageCreate', (m) => {
    if (m.author.bot) return
    const answer = 'Pô Pedro!'
    if ((m.content.toLowerCase().includes('pô pedro'))
        || (m.content.toLowerCase().includes('pô,pedro'))
        || (m.content.toLowerCase().includes('pô, pedro'))
        || (m.content.toLowerCase()).includes('pô <@' + constants.PEDRO_ID)
        || ((m.content.toLowerCase().includes('pô ')) && m.author.id === constants.PEDRO_ID)) {

        logDebug(`${m.author.username} said "${m.content}"`)
        m.channel.send(answer)
        logDebug(`I dutifully replied "${answer}"`)
    }

})

// Ready to start the client
client.login(auth.token)
