# Program to convert an XML
# file to JSON format

# xmltodict module must be installed in the environment:
# pip install xmltodict
import xmltodict
import json
import sys

# command-line argument is the path to the XML file
fileName = sys.argv[1]

# open the input XML file and read
# data in python dictionary format
with open(fileName, 'rb') as xml_file:

	# parameters modify default xmltodict behavior
	# attr_prefix default is '@'
	# cdata_key default is '#text'
	# both create difficulties when using JSON in JS
	# (since they must be used as array string indexes),
	# so we get rid of them here
	data_dict = xmltodict.parse(xml_file.read(), attr_prefix='', cdata_key='text')
	xml_file.close()

    # prints JSON to stdout
	print(json.dumps(data_dict))
