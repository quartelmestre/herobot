# Program to calculate the SHA1 hash of a file
# https://stackoverflow.com/questions/22058048/hashing-a-file-in-python

import sys
import hashlib

# BUF_SIZE is totally arbitrary
# file read in 64kb chunks
BUF_SIZE = 65536

sha1 = hashlib.sha1()

with open(sys.argv[1], 'rb') as f:
	while True:
		data = f.read(BUF_SIZE)
		if not data:
			break
		sha1.update(data)

print("{0}".format(sha1.hexdigest()))
