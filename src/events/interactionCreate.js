'use strict'

// https://discord.js.org/#/docs/main/stable/class/Client?scrollTo=e-interactionCreate

import 'dotenv/config.js'

import { logDebug, logError } from '../library/_logger.js'

import { colorRed } from '../library/_styling.js'

// this is the main bot routine
// when a slash command is executed, this generates an interaction of type APPLICATION_COMMAND
// when an option from a menu is selected, this generates an interaction of type SELECT_MENU
// when a button from a message is pressed, this generates an interaction of type BUTTON

export default {
    name: 'interactionCreate',
    once: false,
    async execute (i) {
        logDebug(`${i.user.tag} in #${i.channel.name} triggered an interaction type ${i.type}.`)

        // sanity check
        // is there a gamestate associated to this guild?
        const game = i.client.games.get(i.member.guild.id)
        if (!game) {

            if (i.commandName !== 'start') {

                let message = `Guild ${i.member.guild.name} has no game associated.`
                if (i.member.permissions.has('ADMINISTRATOR')) {
                    message += ' Please use the /loadgame command to start a game.'
                } else {
                    message += ' Please ask a guild admin to start the game with the /loadgame command.'
                }
                await i.reply({
                    content: colorRed(message)
                })
                return
            }
        }
        
        // if this interaction is a command
        // we pass it on to the corresponding command code
        if (i.isCommand()) {
            // this is the collection of commands that was created during application start
            const command = i.client.commands.get(i.commandName)
        
            // if this interaction is not a known command, just return
            if (!command) return

            try {
                await command.default.execute(i)
            } catch (error) {
                logError(`/${command.default.data.name} failed with ${error}`)
                console.error(error)
                await i.reply({ content: 'There was an error while executing this command!', ephemeral: true })
            }
        } else {
            // since this is not a command, let it pass through and
            // continue processing elsewhere
            i.deferUpdate()
        }
    }
}
