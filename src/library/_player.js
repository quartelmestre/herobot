'use strict'

// import { Collection } from 'discord.js'

export class Player {
    constructor (snowflake) {
        this.id = snowflake
        this.name = null
        this.time = Date.now() // creation date
        this.active = { // the active character is used for subsequent commands
            character: null, // only one at a time may be active
            targets: [], // these are the current targets of any character action
            thread: null // this is the thread used to communicate with the player
        }
        this.notes = null
        this.tags = null
    }
}
