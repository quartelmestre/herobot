'use strict'

// timezone configured in the system environment (docker-compose.yaml)

// const today = new Date()
// const dd = String(today.getDate()).padStart(2, '0')
// const mm = String(today.getMonth() + 1).padStart(2, '0') // Date() tem meses de 0 a 11
// const yyyy = today.getFullYear()
// const hh = String(today.getHours()).padStart(2, '0')
// const mn = String(today.getMinutes()).padStart(2, '0')
// const ss = String(today.getSeconds()).padStart(2, '0')
// const content = `${yyyy}-${mm}-${dd} ${hh}:${mn}:${ss} ${message}`

export function dateStringISO (epoch) { // string such as returned by Date.now()
    const date = new Date(epoch)
    return date.toLocaleString('sv-SE', { timeZone: process.env.TZ })
}

export function dayStringISO (epoch) { // string such as returned by Date.now()
    const date = new Date(epoch)
    return date.toLocaleDateString('sv-SE', { timeZone: process.env.TZ })
}

export function timeStringISO (epoch) { // string such as returned by Date.now()
    const date = new Date(epoch)
    return date.toLocaleTimeString('sv-SE', { timeZone: process.env.TZ })
}

export function dateStringBR (epoch) { // string such as returned by Date.now()
    const date = new Date(epoch)
    return date.toLocaleString('pt-BR', { timeZone: process.env.TZ })
}
