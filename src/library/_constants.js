'use strict'

export const constants = {

    GAMES_DIR: 'games',

    COMMANDS_DIR: 'commands',

    EVENTS_DIR: 'events',

    SCRIPTS_DIR: 'scripts',

    TMP_CHAR_FILE: 'tmp.hdc',

    XML2JSON: 'xml2json.py',

    HASH: 'hash.py',

    TIMEOUT: 10000,

    ROLE_GM: 'HeroGM',

    PEDRO_ID: '438800761900498946',

    NUMBERS: [ 
        '0️⃣', '1️⃣', '2️⃣', '3️⃣', '4️⃣',
        '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣',
        '🔟', '#️⃣', '*️⃣', '➡️', '⬅️',
        '⬆️', '⬇️', '↗️', '↘️', '↙️',
        '↖️', '▶️', '◀️', '🔼', '🔽' 
    ]

}
