'use strict'

import { MessageEmbed, MessageAttachment } from 'discord.js'

import { d6 } from '../library/_hero.js'

import { logDebug } from '../library/_logger.js'

import { BlankFace } from '../library/_styling.js'

// import { Collection } from 'discord.js'

export class Character {
    constructor (snowflake) {
        this.id = snowflake
        this.time = Date.now() // creation date
        this.visibility = { // Collection elements are player snowflakes
            full: [], // these players can see all character information
            partial: [] // these players can see only basic character information
        }
        this.control = [] // these players can control this character (make rolls, for instance)
        this.notes = null
        this.gmNotes = null
        this.tags = null
        this.hash = null // this is the hash of the original file, to check whether it has been already uploaded
        this.sheet = null
    }

    // this function makes sure that crucial information in the character sheet is
    // defined and initialized -- for instance, making sure that there is a character name,
    // that some fields are at least blank instead of null, numeric fields are numbers and
    // not strings, and so forth
    // 
    // whenever a new field is used in any code, include here any needed sanity checks
    //
    // this function should be called when a HDC file is loaded, and also when
    // loading the gamestate file (to ensure that characters already loaded include
    // new sanity checks)
    sanitize () {
        // CHARACTER.CHARACTER_INFO.CHARACTER_NAME
        if (typeof this.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME === 'undefined' 
            || this.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME === null
            || this.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME === ''
            || this.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME === '') {

            this.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME = '(no name)'
        }

        // CHARACTER.CHARACTER_INFO.ALTERNATE_IDENTITIES
        if (typeof this.sheet.CHARACTER.CHARACTER_INFO.ALTERNATE_IDENTITIES === 'undefined' 
            || this.sheet.CHARACTER.CHARACTER_INFO.ALTERNATE_IDENTITIES === null
            || this.sheet.CHARACTER.CHARACTER_INFO.ALTERNATE_IDENTITIES === ' '
            || this.sheet.CHARACTER.CHARACTER_INFO.ALTERNATE_IDENTITIES === '') {

            this.sheet.CHARACTER.CHARACTER_INFO.ALTERNATE_IDENTITIES = '(no alternate identities)'
        }

        // CHARACTER.CHARACTER_INFO.PERSONALITY
        if (typeof this.sheet.CHARACTER.CHARACTER_INFO.PERSONALITY === 'undefined' 
            || this.sheet.CHARACTER.CHARACTER_INFO.PERSONALITY === null
            || this.sheet.CHARACTER.CHARACTER_INFO.PERSONALITY === ' '
            || this.sheet.CHARACTER.CHARACTER_INFO.PERSONALITY === '') {

            this.sheet.CHARACTER.CHARACTER_INFO.PERSONALITY = '(not informed)'
        }

        // CHARACTER.CHARACTER_INFO.QUOTE
        if (typeof this.sheet.CHARACTER.CHARACTER_INFO.QUOTE === 'undefined' 
            || this.sheet.CHARACTER.CHARACTER_INFO.QUOTE === null
            || this.sheet.CHARACTER.CHARACTER_INFO.QUOTE === ' '
            || this.sheet.CHARACTER.CHARACTER_INFO.QUOTE === '') {

            this.sheet.CHARACTER.CHARACTER_INFO.QUOTE = '(not informed)'
        }

        // CHARACTER.CHARACTER_INFO.BACKGROUND
        if (typeof this.sheet.CHARACTER.CHARACTER_INFO.BACKGROUND === 'undefined' 
            || this.sheet.CHARACTER.CHARACTER_INFO.BACKGROUND === null
            || this.sheet.CHARACTER.CHARACTER_INFO.BACKGROUND === ' '
            || this.sheet.CHARACTER.CHARACTER_INFO.BACKGROUND === '') {

            this.sheet.CHARACTER.CHARACTER_INFO.BACKGROUND = '(not informed)'
        }

        // CHARACTER.IMAGE
        if (typeof this.sheet.CHARACTER.IMAGE === 'undefined') {
            this.sheet.CHARACTER.IMAGE = {
                FileName: BlankFace.filename,
                FilePath: BlankFace.filename,
                text: BlankFace.image
            }
        }

        // CHARACTER.POWERS
        if (typeof this.sheet.CHARACTER.POWERS === 'undefined' 
            || this.sheet.CHARACTER.POWERS === null
            || this.sheet.CHARACTER.POWERS === ' '
            || this.sheet.CHARACTER.POWERS === '') {

            this.sheet.CHARACTER.POWERS = {
                POWER: []
            }
        }

        if ('text' in this.sheet.CHARACTER.IMAGE) {
            if (typeof this.sheet.CHARACTER.IMAGE.FileName === 'undefined'
                || this.sheet.CHARACTER.IMAGE.FileName === null
                || this.sheet.CHARACTER.IMAGE.FileName === ' '
                || this.sheet.CHARACTER.IMAGE.FileName === '') {
                this.sheet.CHARACTER.IMAGE.FileName = 'file'
            }
            // the filename will be used when showing the image
            // this ensures lowercase, and replace blank spaces for underlines
            this.sheet.CHARACTER.IMAGE.FileName = this.sheet.CHARACTER.IMAGE.FileName.toLowerCase().replace(/ /g, '_')
        }

        logDebug(`Character ${this.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME} has been sanitized.`)

    }

    // creates an embed with basic character information and image
    show () {

        let attach

        const embed = new MessageEmbed()
            .setTitle(this.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME)
            .setDescription(this.sheet.CHARACTER.CHARACTER_INFO.ALTERNATE_IDENTITIES)
            
        embed.addFields(
            { name: 'Background', value: this.sheet.CHARACTER.CHARACTER_INFO.BACKGROUND.substring(0,255)},
            { name: 'Personality', value: this.sheet.CHARACTER.CHARACTER_INFO.PERSONALITY.substring(0,255)},
            { name: 'Quote', value: this.sheet.CHARACTER.CHARACTER_INFO.QUOTE.substring(0,255)}
        )

        embed.setTimestamp()
        // embed.setFooter({ text: 'Some footer text here' })

        const imageText = this.sheet.CHARACTER.IMAGE.text
        const buffer = Buffer.from(imageText, 'base64')
        attach = new MessageAttachment(buffer, this.sheet.CHARACTER.IMAGE.FileName)
        embed.setImage(`attachment://${this.sheet.CHARACTER.IMAGE.FileName}`)
        return { embed, attach }
    }

    rollPER () {
        const e = new MessageEmbed()
    
        const levelsINT = parseInt(this.sheet.CHARACTER.CHARACTERISTICS.INT.LEVELS)
        const INT = 10 + levelsINT
        e.addFields(
            { name: 'Base roll', value: `INT ${INT}`}
        )
    
        let modifier = 0
        let result = ''
        if (typeof this.sheet.CHARACTER.POWERS.POWER !== 'undefined') {
            const powers = this.sheet.CHARACTER.POWERS.POWER
            powers.forEach(p => {
                if (p.XMLID === 'ENHANCEDPERCEPTION') {
                    modifier += parseInt(p.LEVELS)
                    result += `\n. ${p.ALIAS} ${p.LEVELS} (${p.OPTION_ALIAS})`
                }
            })
        }
        if (modifier !== 0) {
            e.addFields(
                { name: 'Modifiers', value: result}
            )
        }
    
        const PER = INT + modifier
        e.setTitle(`PER ${PER}-`)
    
        const dice = [d6(), d6(), d6()]
        const diceTotal = dice[0] + dice[1] + dice[2]
        e.addFields(
            { name: 'Dice', value: `${dice[0]} + ${dice[1]} + ${dice[2]} = ${diceTotal}`}
        )
    
        if (diceTotal < PER) {
            e.setFooter({ text: `Success by ${PER - diceTotal}.` })
        } else if (diceTotal === PER) {
            e.setFooter({ text: 'Success (on the dot).' })
        } else {
            e.setFooter({ text: `Failed by ${PER - diceTotal}.` })
        }

        return (e)
    }
}
