'use strict'

// Hero RPG helper functions

export function d6 () {
    const getRandomIntInclusive = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min
    return(getRandomIntInclusive(1,6))
}
