'use strict'

// GameState includes all permanent game information on characters and players

import { constants } from '../library/_constants.js'

import { logDebug, logError, logInfo } from '../library/_logger.js'

import { dateStringISO } from '../library/_date.js'

import { Collection } from 'discord.js'

import { existsSync, readFileSync, writeFileSync } from 'node:fs'
import { join } from 'node:path'
import * as url from 'url'

import { Character } from '../library/_character.js'

import { Player } from '../library/_player.js'

// path for this file
const localPath = url.fileURLToPath(new URL('.', import.meta.url))

export class GameState {
    constructor () {
        this.time = Date.now()
        this.guild = null
        this.title = null
        this.players = new Collection()
        this.characters = new Collection()
    }

    // writes the current game state to a file
    writeState () {
        const now = Date.now()
        const gameFile = join(localPath, '..', constants.GAMES_DIR, now.toString() + '.json')

        // create an old files collector
        // whenever a new game file is written, old files are deleted
        // until there are at most X of them
        // define X in the .env file

        // by the way -- let's define two .env files
        // one of them for credentials only
        // the other one may be versioned
        // const gameOldFile = join(localPath, '..', constants.GAME_DIR, this.guild, '-previous.json')
        
        // if there is an old backup file, it is deleted
        // using Sync function ensures that this will be done before creating the new backup file
        // if (existsSync(gameOldFile)) {
        //     unlinkSync(gameOldFile)
        // }

        // if there is an old main file, it is renamed and becomes the new backup file
        // if (existsSync(gameFile)) {
        //     renameSync(gameFile, gameOldFile)
        // }

        // now, we prepare the gamestate data to write
        // the present timestamp is used both in the file name and in the content
        // NB: toWrite has arrays instead of collections
        const toWrite = {
            time: now,
            guild: this.guild,
            title: this.title,
            players: [],
            characters: []
        }

        // since the collection keys are always also included in
        // the value part of our collections, we may forgo
        // adding the keys, and we just push the values part
        // to each array in toWrite
        for (const p of this.players.values()) {
            toWrite.players.push(p)
        }
        for (const c of this.characters.values()) {
            toWrite.characters.push(c)
        }

        // we can now create a JSON file
        // the third argument specifies that it will be a multiline JSON file,
        // with 2 spaces for each level
        // -- both in order to improve readability,
        // and also to allow for text editors with character limits on lines
        const data = JSON.stringify(toWrite, null, 2)
        writeFileSync(gameFile, data)
    }

    // reads the game state from a previous file
    readState (f) {
        const gameFile = join(localPath, '..', constants.GAMES_DIR, f)
        try {
            // first, we check to see whether the file exists
            if (!existsSync(gameFile)) throw new Error(`File ${gameFile} not found.`)

            let readData

            try {
                readData = JSON.parse(readFileSync(gameFile))
                logDebug(`File ${gameFile} has been read.`)
            } catch (err) {
                logError(err)
            }

            // characters and players are arrays
            // we convert them to collections;
            // since the key is already in the array (id field),
            // we just use it
            this.time = readData.time
            this.guild = readData.guild
            this.title = readData.title

            this.players = new Collection()
            readData.players.forEach(element => {
                const player = new Player(element.id)
                player.name = element.name
                player.time = element.time
                player.active = element.active
                player.notes = element.notes
                player.tags = element.tags

                // player.sanitize()
                this.players.set(player.id, player)
            })

            this.characters = new Collection()
            readData.characters.forEach(element => {
                const char = new Character(element.id)
                char.time = element.time
                char.visibility = element.visibility
                char.control = element.control
                char.notes = element.notes
                char.gmNotes = element.gmNotes
                char.tags = element.tags
                char.hash = element.hash
                char.sheet = element.sheet

                char.sanitize()
                this.characters.set(char.id, char)
            })

            logInfo(`Loaded gamestate info from ${dateStringISO(this.time)}.`)
        } catch (err) {
            logError(err)
        }
    }
}
