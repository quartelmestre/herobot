'use strict'

import { dateStringISO, timeStringISO } from '../library/_date.js'

import { createLogger, format, transports } from 'winston'

export let logLevel = ''
if (process.env.NODE_ENV === 'production') {
    logLevel = 'info'
} else {
    logLevel = 'debug'
}

const { combine, prettyPrint } = format
const logger = createLogger(
    {
        format: combine(
            // timestamp(),
            prettyPrint()
        ),
        transports: [new transports.Console({
            level: logLevel,
            format: format.combine(
                format.colorize(),
                format.simple()
            )
        })]
    }
)

logger.level = logLevel

// messages are written to the server console, with a timestamp
// timezone configured in the system environment (docker-compose.yml)

export function logInfo (message) {
    return (logger.info(` ${dateStringISO(Date.now())} ${message}`))
}

export function logError (message) {
    return (logger.error(`${timeStringISO(Date.now())} ${message}`))
}

export function logDebug (message) {
    return (logger.debug(`${timeStringISO(Date.now())} ${message}`))
}
