'use strict'

import { constants } from '../library/_constants.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { styles } from '../library/_styling.js'

import { logError } from '../library/_logger.js'

// import { rollPER } from '../library/_hero.js'

import { MessageAttachment, MessageEmbed, MessageActionRow, MessageButton } from 'discord.js'

export default {
    data: new SlashCommandBuilder()
        .setName('player')
        .setDescription('Player commands.'),
    async execute (i) {

        const gs = i.client.games.get(i.guild.id)
        const thisPlayer = gs.players.get(i.user.id)

        const embed = new MessageEmbed()
            .setColor(styles.private)
            .setTitle('')
            // .setFooter({ text: 'Select a button:' })
            .setAuthor({ name: '👤', iconURL: i.user.displayAvatarURL({ dynamic: true }) })

        const attach = []
        const buttons = []

        // embed shows current active char and targets
        // embed
        // embed.setThumbnail(i.user.displayAvatarURL({ dynamic: true }))
        if (thisPlayer.active.character !== null) {
            const nowActive = gs.characters.get(thisPlayer.active.character)
            const imageText = nowActive.sheet.CHARACTER.IMAGE.text
            const buffer = Buffer.from(imageText, 'base64')
            attach.push(new MessageAttachment(buffer, nowActive.sheet.CHARACTER.IMAGE.FileName))
            embed.setThumbnail(`attachment://${nowActive.sheet.CHARACTER.IMAGE.FileName}`)
        }

        let targets = ''
        let count = 0
        thisPlayer.active.targets.forEach(c => {
            targets += `${gs.characters.get(c).sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME}, `
            count ++
        })
        targets = targets.slice(0, -2)
        if (count === 0) {
            targets = 'No current targets.'
        } else if (count === 1) {
            targets = `Current target: ${targets}`
        } else {
            targets = `Current targets: ${targets}`
        }

        let list
        const rowCharacters = new MessageActionRow()
        if (thisPlayer.active.character === null) {
            list = ''
        } else {
            rowCharacters.addComponents(
                new MessageButton()
                    .setCustomId('btnDeActivateCharacter')
                    .setEmoji('⏹')
                    .setStyle('SECONDARY')
                    .setDisabled(false)
            )
            list = '⏹ (deactivate)'
        }

        count = 0

        for (const char of gs.characters.values()) {
            if (char.control.includes(i.user.id)
            || i.member.roles.cache.some(role => role.name === constants.ROLE_GM)) {

                list += `\n${constants.NUMBERS[count]} `
                list += char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME
                if (char.id === thisPlayer.active.character) {
                    rowCharacters.addComponents(new MessageButton()
                        .setCustomId(`bChar.${char.id}`)
                        .setEmoji(constants.NUMBERS[count])
                        .setStyle('SECONDARY')
                        .setDisabled(true)
                    )
                } else {
                    rowCharacters.addComponents(new MessageButton()
                        .setCustomId(`bChar.${char.id}`)
                        .setEmoji(constants.NUMBERS[count])
                        .setStyle('SECONDARY')
                        .setDisabled(false)
                    )
                }
                count++
            }
        }

        const rowButtons = new MessageActionRow()
        rowButtons.addComponents(
            new MessageButton()
                .setCustomId('btnSelectTargets')
                .setEmoji('🎯')
                .setStyle('SECONDARY')
        )
        rowButtons.addComponents(
            new MessageButton()
                .setCustomId('btnTalk')
                .setEmoji('🗣')
                .setStyle('SECONDARY')
        )
        rowButtons.addComponents(
            new MessageButton()
                .setCustomId('btnEmote')
                .setEmoji('🎭')
                .setStyle('SECONDARY')
        )
        rowButtons.addComponents(
            new MessageButton()
                .setCustomId('btnRoll')
                .setEmoji('🎲')
                .setStyle('SECONDARY')
        )

        buttons.push(rowCharacters)
        if (thisPlayer.active.character !== null) {
            embed.setFooter({ text: 'Use the buttons below to change the active character, or to act with it.' })
            embed.addField(gs.characters.get(thisPlayer.active.character).sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME, targets)
            embed.addFields(
                { name: 'Activation', value: list},
                { name: 'Actions', value: '🎯 select target\n🗣 talk\n🎭 emote\n🎲 roll'}
            )
            buttons.push(rowButtons)
        } else {
            embed.setFooter({ text: 'Use the buttons below to activate a character.' })
            embed.addFields(
                { name: 'Activation', value: list},
            )
        }

        const messageFilter = m => {
            return m.type === 'APPLICATION_COMMAND'
                && m.message.interaction.id === i.id
        }
        const messageCollector = i.channel.createMessageCollector({
            messageFilter, 
            time: constants.TIMEOUT
        })
            .on('end', () => {
                // after timeout, remove buttons from message
                embed.setFooter({ text: '' })
                i.editReply({ embeds: [embed], components: [] })
            })
        await i.deferReply()

        await i.editReply({
            embeds: [embed],
            components: buttons,
            files: attach,
            ephemeral: true
        })
            .then(() => {
                // here the interaction has already been replied to

                // the conditions are necessary to ensure
                // that only the selection from this command will be collected
                // even if other games are in progress at the same time
                const selectFilter = m => {
                    m.deferUpdate()
                    return (m.componentType === 'BUTTON')
                        && m.message.interaction.id === i.id
                }

                const selectCollector = i.channel.createMessageComponentCollector({
                    selectFilter,
                    time: constants.TIMEOUT
                })

                selectCollector.on('collect', c => {

                    // if the user clicked on a button,
                    // we can stop the message collector,
                    // which will delete the buttons
                    messageCollector.stop()

                    const selectedButton = c.customId.split('.')[0]

                    switch (selectedButton) {
                    case 'bChar': {
                        const selectedChar = c.customId.split('.')[1]
                        i.client.games.get(i.guild.id).players.get(i.user.id).active.character = selectedChar
                        embed.setFooter({ text: `Your active character is now ${gs.characters.get(selectedChar).sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME}.` })
                        i.editReply({ embeds: [embed], components: [] })
                
                        break
                    }

                    case 'btnDeActivateCharacter': {
                        i.client.games.get(i.guild.id).players.get(i.user.id).active.character = null
                        i.client.games.get(i.guild.id).players.get(i.user.id).active.targets = []
                        embed.setFooter({ text: 'You now have no active character' })
                        i.editReply({ embeds: [embed], components: [] })
                        break
                    }
                
                    case 'btnSelectTargets': {
                        i.followUp('Not implemented yet.')
                        break
                    }

                    case 'btnTalk': {
                        i.followUp('Not implemented yet.')
                        break
                    }
                    
                    case 'btnEmote': {
                        i.followUp('Not implemented yet.')
                        break
                    }
                    
                    case 'btnRoll': {
                        const char = gs.characters.get(thisPlayer.active.character)
                        const e = char.rollPER()

                        e.setColor(styles.private)
                        e.setAuthor({ name: '👤', iconURL: i.user.displayAvatarURL({ dynamic: true }) })

                        const imageText = char.sheet.CHARACTER.IMAGE.text
                        const buffer = Buffer.from(imageText, 'base64')
                        const attach = new MessageAttachment(buffer, char.sheet.CHARACTER.IMAGE.FileName)
                        e.setThumbnail(`attachment://${char.sheet.CHARACTER.IMAGE.FileName}`)
                        e.setColor(styles.private)
                        i.followUp({
                            embeds: [e],
                            files: [attach],
                            ephemeral: true
                        })
                        break
                    }
                    
                    default:
                        logError(`Unknown button pressed on /info: ${selectedButton}.`)
                    }
                    selectCollector.stop()
                })

            })

    }
}
