'use strict'

import 'dotenv/config.js'

import { constants } from '../library/_constants.js'

import { dateStringISO } from '../library/_date.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { logError } from '../library/_logger.js'

import { PoPedro, colorRed, colorYellow, styles } from '../library/_styling.js'

import { MessageAttachment, MessageEmbed, MessageActionRow, MessageButton, MessageSelectMenu } from 'discord.js'

export default {
    data: new SlashCommandBuilder()
        .setName('gm')
        .setDescription('Gamemaster commands.'),
    async execute (i) {

        // test if the user is a GM for this game
        if (!i.member.roles.cache.some(role => role.name === constants.ROLE_GM)) {
            await i.reply(colorRed('Only the gamemaster may use this command.'))
            return
        }

        const gs = i.client.games.get(i.guild.id)

        const embed = new MessageEmbed()
            .setColor(styles.gm)
            .setTitle('GameMaster commands')
            .setFooter({ text: 'Select a button:' })

        const rows = []

        const rowButtons = new MessageActionRow()

        rowButtons.addComponents(
            new MessageButton()
                .setCustomId('btnRollPER')
                .setEmoji('👁')
                .setStyle('SECONDARY')
                .setLabel('PER')
        )

        rowButtons.addComponents(
            new MessageButton()
                .setCustomId('btnPoPedro')
                .setEmoji('🤦🏻‍♂️')
                .setStyle('SECONDARY')
                .setLabel('PP')
        )

        const rowCharacters = new MessageActionRow()
        const menu = new MessageSelectMenu()
            .setCustomId('menuActivateCharacter')
            .setMinValues(0)
        
        for (const char of gs.characters.values()) {
            menu.addOptions({
                label: char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME,
                value: char.id,
                description: dateStringISO(char.time)
            })
        }
        if (menu.options.length === 0) {
            embed.setDescription(colorYellow('You do not control any characters.'))
        } else {
            menu.setMaxValues(menu.options.length)
            rowCharacters.addComponents(menu)
            embed.setFooter({ text: 'Select one or more characters, and click a button:' })
            rows.push(rowCharacters)
            rows.push(rowButtons)
        }

        const messageFilter = m => {
            return m.type === 'APPLICATION_COMMAND'
                && m.message.interaction.id === i.id
        }
        const messageCollector = i.channel.createMessageCollector({
            messageFilter, 
            time: constants.TIMEOUT
        })
            .on('end', () => {
                // after timeout, remove buttons from message
                i.editReply({ components: [] })
                messageCollector.stop()
            })

        i.reply({
            embeds: [embed],
            components: rows,
            ephemeral: true
        })
            .then(() => {
                // here the interaction has already been replied to

                // the conditions are necessary to ensure
                // that only the selection from this command will be collected
                // even if other games are in progress at the same time
                const selectFilter = m => {
                    m.deferUpdate()
                    return (m.componentType === 'BUTTON' || m.componentType === 'SELECT_MENU')
                        && m.message.interaction.id === i.id
                }

                const selectCollector = i.channel.createMessageComponentCollector({
                    selectFilter, 
                    time: constants.TIMEOUT
                })

                selectCollector.on('collect', c => {

                    // mudar a interface
                    // comando /select mostra apenas o menu e espera a seleção
                    // comando /gm mostra quem está selecionado e espera botão
                    //     - unselect limpa a seleção
                    // => todo comando passa a mostrar quem está selecionado (ativo)
                    // se apenas um char é controlado, isso é automático
                    // incluir um teste no início de cada comando
                    console.log(c)

                    // we delete both the menu and the buttons
                    // i.editReply({ components: [] })

                    const selections = selectCollector.collected

                    console.log('SELECTIONS')
                    console.log(selections)

                    let selectedChar
                    let selectedButton
                    for (const c of selections.values()) {
                        // we go through all selections
                        // since the user may select a character and
                        // then change selection before pressing a button
                        // we go with the last one selected
                        if (c.componentType === 'SELECT_MENU') {
                            selectedChar = c.values[0]
                        } else {
                            selectedButton = c.customId
                        }
                    }

                    console.log('BUTTON')
                    console.log(selectedButton)
                    if (typeof selectedButton === 'undefined') return

                    // test if there was any character selected
                    // before a button press ended the command
                    // if none was selected, just stop the collector and return
                    if (typeof selectedChar === 'undefined') {
                        selectCollector.stop()
                        return
                    }

                    switch (selectedButton) {

                    case 'btnRollPER': {
                        console.log('rolling PER')
                        break
                    }

                    case 'btnPoPedro': {
                
                        const embed = new MessageEmbed()
                            .setColor(styles.gm)
                            .setImage(`attachment://${PoPedro.filename}`)
                        const buffer = Buffer.from(PoPedro.image, 'base64')
                        const attach = new MessageAttachment(buffer, PoPedro.filename)
                            
                        i.followUp({
                            embeds: [embed],
                            files: [attach],
                            ephemeral: false
                        })
                
                        break
                    }
                
                    default:
                        logError(`Unknown button pressed on /info: ${selectedButton}`)
                    }
                })
            })

    }
}
