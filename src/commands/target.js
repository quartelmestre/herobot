'use strict'

import { constants } from '../library/_constants.js'

import { dateStringISO } from '../library/_date.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { colorRed, styles } from '../library/_styling.js'

import { logError } from '../library/_logger.js'

import { MessageAttachment, MessageEmbed, MessageActionRow, MessageSelectMenu } from 'discord.js'

export default {
    data: new SlashCommandBuilder()
        .setName('target')
        .setDescription('Selects targets for other commands.'),
    async execute (i) {

        const gs = i.client.games.get(i.guild.id)
        const thisPlayer = gs.players.get(i.user.id)

        if (thisPlayer.active.character === null) {
            await i.reply(colorRed('No active character selected. Use the /player command.'))
            return
        }

        const embed = new MessageEmbed()
            .setColor(styles.private)
            .setTitle('')
            .setFooter({ text: 'Select the targets for the active character:' })
            .setAuthor({ name: '👤', iconURL: i.user.displayAvatarURL({ dynamic: true }) })

        const attach = []

        // embed shows current active char and targets
        // embed
        // embed.setThumbnail(i.user.displayAvatarURL({ dynamic: true }))
        if (thisPlayer.active.character !== null) {
            const nowActive = gs.characters.get(thisPlayer.active.character)
            const imageText = nowActive.sheet.CHARACTER.IMAGE.text
            const buffer = Buffer.from(imageText, 'base64')
            attach.push(new MessageAttachment(buffer, nowActive.sheet.CHARACTER.IMAGE.FileName))
            embed.setThumbnail(`attachment://${nowActive.sheet.CHARACTER.IMAGE.FileName}`)
        }

        let current = ''
        let count = 0
        thisPlayer.active.targets.forEach(c => {
            current += `${gs.characters.get(c).sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME}, `
            count ++
        })
        current = current.slice(0, -2)
        if (count === 0) {
            current = 'No current targets.'
        } else if (count === 1) {
            current = `Current target: ${current}`
        } else {
            current = `Current targets: ${current}`
        }

        embed.addField(gs.characters.get(thisPlayer.active.character).sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME, current)

        const menu = new MessageSelectMenu()
            .setCustomId('menuActivateCharacter')
            .setMinValues(0)

        for (const char of gs.characters.values()) {
            // if ((char.visibility.full.includes(i.user.id)
            // || char.visibility.partial.includes(i.user.id))
            // && char.id !== thisPlayer.active.character) {
            
                menu.addOptions({
                    label: char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME,
                    value: char.id,
                    description: dateStringISO(char.time)
                })
    
            // }
        }

        if (menu.options.length === 0) {
            embed.setFooter({ text: 'There are no visible targets.' })
            await i.reply({
                embeds: [embed],
                files: attach,
                ephemeral: true
            })
    
            return
        }

        const rowCharacters = new MessageActionRow()
        menu.setMaxValues(menu.options.length)
        rowCharacters.addComponents(menu)

        const messageFilter = m => {
            return m.type === 'APPLICATION_COMMAND'
                && m.message.interaction.id === i.id
        }
        const messageCollector = i.channel.createMessageCollector({
            messageFilter, 
            time: constants.TIMEOUT
        })
            .on('end', () => {
                // after timeout, remove buttons from message
                embed.setFooter({ text: '' })
                i.editReply({ embeds: [embed], components: [] })
            })
        await i.deferReply()

        await i.editReply({
            embeds: [embed],
            components: [rowCharacters],
            files: attach,
            ephemeral: true
        })
            .then(() => {
                // here the interaction has already been replied to

                // the conditions are necessary to ensure
                // that only the selection from this command will be collected
                // even if other games are in progress at the same time
                const selectFilter = m => {
                    m.deferUpdate()
                    console.log('selectFilter')
                    console.log(m.componentType)
                    return (m.componentType === 'SELECT_MENU')
                        && m.message.interaction.id === i.id
                }

                const selectCollector = i.channel.createMessageComponentCollector({
                    selectFilter,
                    time: constants.TIMEOUT
                })

                selectCollector.on('collect', c => {

                    console.log('selectCollector')
                    console.log(c)
                    // if the user clicked on a button,
                    // we can stop the message collector,
                    // which will delete the buttons
                    messageCollector.stop()

                    // array c.values has the ids of selected targets

                    const selectedButton = c.customId.split('.')[0]

                    switch (selectedButton) {
                    case 'bChar': {
                        const selectedChar = c.customId.split('.')[1]
                        i.client.games.get(i.guild.id).players.get(i.user.id).active.character = selectedChar
                        embed.setFooter({ text: `Your active character is now ${gs.characters.get(selectedChar).sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME}.` })
                        i.editReply({ embeds: [embed], components: [] })
                
                        break
                    }

                    case 'btnDeActivateCharacter': {
                        i.client.games.get(i.guild.id).players.get(i.user.id).active.character = null
                        i.client.games.get(i.guild.id).players.get(i.user.id).active.targets = []
                        embed.setFooter({ text: 'You now have no active character' })
                        i.editReply({ embeds: [embed], components: [] })
                        break
                    }
                
                    case 'btnSelectTargets': {
                        i.followUp('Not implemented yet.')
                        break
                    }

                    case 'btnTalk': {
                        i.followUp('Not implemented yet.')
                        break
                    }
                    
                    case 'btnEmote': {
                        i.followUp('Not implemented yet.')
                        break
                    }
                    
                    case 'btnRoll': {
                        const char = gs.characters.get(thisPlayer.active.character)
                        const e = char.rollPER()

                        e.setColor(styles.private)
                        e.setAuthor({ name: '👤', iconURL: i.user.displayAvatarURL({ dynamic: true }) })

                        const imageText = char.sheet.CHARACTER.IMAGE.text
                        const buffer = Buffer.from(imageText, 'base64')
                        const attach = new MessageAttachment(buffer, char.sheet.CHARACTER.IMAGE.FileName)
                        e.setThumbnail(`attachment://${char.sheet.CHARACTER.IMAGE.FileName}`)
                        e.setColor(styles.private)
                        i.followUp({
                            embeds: [e],
                            files: [attach],
                            ephemeral: true
                        })
                        break
                    }
                    
                    default:
                        logError(`Unknown button pressed on /info: ${selectedButton}.`)
                    }
                    selectCollector.stop()
                })

            })

    }
}
