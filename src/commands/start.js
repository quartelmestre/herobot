'use strict'

import 'dotenv/config.js'

import { constants } from '../library/_constants.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { colorRed } from '../library/_styling.js'

export default {
    data: new SlashCommandBuilder()
        .setName('start')
        .setDescription('Starts a gaming session.'),
    async execute (i) {

        if (!i.member.roles.cache.some(role => role.name === constants.ROLE_GM)) {
            await i.reply(colorRed('Only the gamemaster may use this command.'))
            return
        }

        i.deferReply()

        for (const p of i.client.games.get(i.guild.id).players.values()) {
            // create new thread
            const newThread = await i.channel.threads.create({
                name: p.name,
                autoArchiveDuration: 1440, // minutes of inactivity
                reason: 'Hero player thread'
            })
            await newThread.join()
            await newThread.members.add(p.id)
            await newThread.members.add(i.member.user.id)
            i.client.games.get(i.guild.id).players.get(p.id).active.thread = newThread.name
        }

        for (const c of i.client.games.get(i.guild.id).characters.values()) {
            const controller = c.control[0]
            if (typeof controller !== 'undefined') {
                const threadName = i.client.games.get(i.guild.id).players.get(controller).active.thread
                const thread = i.channel.threads.cache.find(t => t.name === threadName)
                i.client.games.get(i.guild.id).players.get(controller).active.character = c.id
                thread.send(`Your character ${c.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME} is now active.`)
            }
        }

        await i.editReply({
            content: 'Session started.',
            ephemeral: true
        })
    }
}
