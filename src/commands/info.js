'use strict'

import 'dotenv/config.js'

import { constants } from '../library/_constants.js'

import { styles } from '../library/_styling.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { MessageEmbed, MessageActionRow, MessageButton } from 'discord.js'

export default {
    data: new SlashCommandBuilder()
        .setName('info')
        .setDescription('Displays information on characters.'),
    execute (i) {

        const gs = i.client.games.get(i.guild.id)

        const embed = new MessageEmbed()
            .setColor(styles.private)
            .setTitle('Information')
            .setAuthor({ name: i.user.username, iconURL: i.user.displayAvatarURL({ dynamic: true }) })
            .setThumbnail(i.user.displayAvatarURL({ dynamic: true }))
            .setFooter({ text: 'Select a button:' })

        let list = ''
        let count = 0
        const buttons = []

        for (const char of gs.characters.values()) {
            if (char.visibility.full.includes(i.user.id)
            || char.visibility.partial.includes(i.user.id)
            || i.member.roles.cache.some(role => role.name === constants.ROLE_GM)) {

                list += `\n\n${constants.NUMBERS[count]} `
                list += char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME
                let controllers = ''
                char.control.forEach(element => {
                    controllers += `${gs.players.get(element).name}, `
                })
                controllers = controllers.slice(0, -2)
                if (controllers.length > 0) {
                    list += `\n\u200b(${controllers})`
                }

                buttons.push(new MessageButton()
                    .setCustomId(`bChar.${char.id}`)
                    .setEmoji(constants.NUMBERS[count])
                    .setStyle('SECONDARY'))

                count++

            }
        }

        const buttonsRows = []

        if (count === 0) {
            list = 'There are no visible characters'
        } else {
            const lim = Math.floor(count / 5) + 1
    
            // maximum 5 buttons per row
            for (let x = 0; x < lim; x ++) {
                buttonsRows.push(new MessageActionRow)
            }
            let n = 0
            buttons.forEach(b => {
                buttonsRows[n].addComponents(b)
                if (buttonsRows[n].length === 5) {
                    n++
                }
            })
        }

        embed.setDescription(list)

        const messageFilter = m => {
            return m.type === 'APPLICATION_COMMAND'
                && m.message.interaction.id === i.id
        }
        const messageCollector = i.channel.createMessageCollector({
            messageFilter, 
            time: constants.TIMEOUT
        })
            .on('end', () => {
                // after timeout, remove buttons from message
                embed.setFooter({ text: '' })
                i.editReply({ embeds: [embed], components: [] })
            })

        i.reply({
            embeds: [embed],
            components: buttonsRows,
            ephemeral: true
        })
            .then(() => {
                // here the interaction has already been replied to

                // the conditions are necessary to ensure
                // that only the selection from this command will be collected
                // even if other games are in progress at the same time
                const selectFilter = m => {
                    return m.componentType === 'BUTTON'
                        && m.message.interaction.id === i.id
                }

                const selectCollector = i.channel.createMessageComponentCollector({
                    selectFilter, 
                    time: constants.TIMEOUT
                })

                selectCollector.on('collect', b => {
                    // b is the button object
                    // b.customId has the name of the button pressed

                    // first, we delete the action row,
                    // preventing further button presses
                    i.editReply({ components: [] })
                    messageCollector.stop()
                    selectCollector.stop()

                    const charShow = b.customId.split('.')[1]
                    const m = gs.characters.get(charShow).show()
                    m.embed.setColor(styles.private)
                    i.followUp({
                        embeds: [m.embed],
                        files: [m.attach],
                        ephemeral: true
                    })

                })
            })

    }
}
