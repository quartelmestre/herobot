'use strict'

import 'dotenv/config.js'

import { constants } from '../library/_constants.js'

import { SlashCommandBuilder } from '@discordjs/builders'

import { colorRed, colorYellow, styles } from '../library/_styling.js'

import { dateStringISO } from '../library/_date.js'

import { GameState } from '../library/_gamestate.js'

import { MessageEmbed, MessageActionRow, MessageSelectMenu } from 'discord.js'

import { readdirSync, readFileSync } from 'node:fs'
import { join } from 'node:path'
import * as url from 'url'

export default {
    data: new SlashCommandBuilder()
        .setName('loadgame')
        .setDescription('Load a game file or start a new game in this server.'),
    async execute (i) {

        // test if the user is a guild admin
        if (!i.member.permissions.has('ADMINISTRATOR')) {
            await i.reply({
                content: colorRed('Only a guild admin may use this command.'),
                ephemeral: true
            })
            return
        }

        // the GM role is created in the guild
        // if it does not exist
        const roleGM = constants.ROLE_GM
        const role = i.guild.roles.cache.find(r => r.name === roleGM )
        if (typeof role === 'undefined') {
            i.guild.roles.create({ name: roleGM })
        }

        const row = new MessageActionRow()

        const menu = new MessageSelectMenu()
            .setCustomId('newgame.' + i.guild.id)
            .addOptions({
                label: 'New game',
                description: 'Start a new game',
                value: 'newgame'
            })

        // let's find the game files
        const localPath = url.fileURLToPath(new URL('..', import.meta.url))
        const gamesPath = join(localPath, constants.GAMES_DIR)
        const gameFiles = readdirSync(gamesPath).filter(file => file.endsWith('.json'))
        // fileList has the choices for the select menu, below
        // only saved games for this guild will be added
        for (const file of gameFiles) {
            const readData = JSON.parse(readFileSync(join(gamesPath, file)))
            if (readData.guild === i.guild.id) {
                menu.addOptions({
                    label: readData.title,
                    description: dateStringISO(readData.time),
                    value: file
                })
            }
        }

        // the menu is now ready, it can be added to the action row
        row.addComponents(menu)

        // the embed has the main text for the command
        const embed = new MessageEmbed()
            .setColor(styles.gm)
            .setTitle('Start a game')

        if (gameFiles.length < 1) {
            embed.setDescription(colorYellow('Do you wish to start a new game?'))
            embed.setFooter({ text: 'No previous game files found.' })
        } else {
            if (gameFiles.length === 1) {
                embed.setFooter({ text: `${gameFiles.length} game file found.`})
            } else {
                embed.setFooter({ text: `${gameFiles.length} game files found.`})
            }
            embed.setDescription(colorYellow('Please make a selection:'))
        }

        // the message components are ready, but we must
        // first ensure that we can disable the selection menu
        // after the timeout period

        // this collector will collect the message we are creating
        // its end event will be triggered by timeout
        // the conditions are necessary to ensure
        // that only this command will be collected
        // even if other games are in progress at the same time
        // but, even if nothing is collected (as expected),
        // the collector timout will remove the components
        // from the interaction reply message
        const messageFilter = m => {
            return m.type === 'APPLICATION_COMMAND'
                && m.message.interaction.id === i.id
        }
        const messageCollector = i.channel.createMessageCollector({
            messageFilter, 
            time: constants.TIMEOUT
        })
            .on('end', () => {
                // this will be triggered by timeout if no selection is made
                // the original interaction reply (i. e., the message with the menu)
                // is edited to remove its components (menus and/or buttons)
                i.editReply({ components: [] })
                messageCollector.stop()
            })

        // the collector is ready and waiting,
        // time to really send in the message
        // to the user
        i.reply({
            embeds: [embed],
            components: [row],
            ephemeral: true
        })
            .then(() => {
                // here the interaction has already been replied to

                // the conditions are necessary to ensure
                // that only the selection from this command will be collected
                // even if other games are in progress at the same time
                const selectFilter = m => {
                    return m.componentType === 'SELECT_MENU'
                        && m.message.interaction.id === i.id
                }

                const selectCollector = i.channel.createMessageComponentCollector({
                    selectFilter, 
                    time: constants.TIMEOUT
                })

                selectCollector.on('collect', s => {
                    // at this moment, s is the selection in the menu
                    // because of the filter, we know it is a response to this command
                    // and not a selection from a simultaneous interaction elsewhere

                    // first, we delete the action row,
                    // preventing further selections
                    i.editReply({ components: [] })
                    messageCollector.stop()
                    selectCollector.stop()

                    let text

                    // if there is already a gameState object
                    // for this guild, we clear it from the games collection
                    if (i.client.games.has(i.guild.id)) {
                        i.client.games.delete(i.guild.id)
                        text = 'The previous game information has been deleted. '
                        text += '(It can be reloaded from a previous file).'
                        i.followUp({ content: colorRed(text), ephemeral: true })
                    }

                    const gs = new GameState()
                    if (s.values[0] === 'newgame') {
                        gs.guild = i.guild.id
                        gs.title = '(no title)'
                        text = 'The new game has been started. '
                        const role = i.guild.roles.cache.find(r => r.name === roleGM )
                        i.member.roles.add(role).catch(console.error)
                        text += 'You are now the GM of the game.'
                    } else {
                        gs.readState(s.values[0])
                        text = 'The selected game file has been loaded.'
                    }

                    // we can now push the new gamestate to the games collection
                    // in the client
                    i.client.games.set(i.guild.id, gs)
                    i.followUp({ content: colorYellow(text), ephemeral: true })
                })
            })
    }
}
