'use strict'

import 'dotenv/config.js'

import { constants } from '../library/_constants.js'

import { logDebug, logError } from '../library/_logger.js'

import { writeFileSync } from 'node:fs'

import { join } from 'node:path'

import * as url from 'url'

import { spawnSync } from 'child_process'

import { Character } from '../library/_character.js'

import { Player } from '../library/_player.js'

import { colorRed, styles } from '../library/_styling.js'

import { SlashCommandBuilder } from '@discordjs/builders'

// path for this file
const localPath = url.fileURLToPath(new URL('.', import.meta.url))

export default {
    data: new SlashCommandBuilder()
        .setName('loadfile')
        .setDescription('Loads a Hero Designer character file.')
        .addAttachmentOption(
            option => option
                .setRequired(true)
                .setName('file')
                .setDescription('Attach the Hero Designer file.')
        ),
    async execute (i) {
        logDebug(`/loadfile command on guild ${i.guild.id}.`)

        const attachment = i.options.getAttachment('file')
        if (!attachment) {
            logError('No attached file found')
            return
        }

        const name = attachment.name
        const url = attachment.url
        logDebug(`Attachment URL: ${url}`)
        logDebug(`Fetching data from file ${name}...`)

        let xml

        try {
            // fetch the file from the external URL
            const response = await fetch(url)

            // if there was an error send a message with the status
            if (!response.ok) {
                logError(`There was an error with fetching the file: ${response.statusText}`)
                return
            }

            logDebug(`File ${name} loaded`)
            // take the response stream and read it to completion
            xml = await response.text()
        } catch (error) {
            logError(error)
        }

        const tmpCharFile = join(localPath, '..', constants.TMP_CHAR_FILE)

        // FE FF: Big Endian
        // FF FE: Little Endian

        if ((Buffer.from(xml[0]).toString('hex') === 'efbfbd')
            && (Buffer.from(xml[1]).toString('hex') === 'efbfbd')) {

            logDebug('File is encoded as UTF-16BE, must be converted to UTF-16 LE')
            const stringBE = Buffer.from(xml)
            stringBE.swap16()
            stringBE[0] = 0xFF
            stringBE[1] = 0xFE

            logDebug(`Writing ${tmpCharFile} in UTF-16 LE encoding`)
            writeFileSync(tmpCharFile, stringBE.toString().substring(stringBE.toString().indexOf('<')))
        } else {
            logDebug('File is encoded as UTF-8 or ASCII, does not need conversion')
            logDebug(`Writing ${tmpCharFile} in UTF-8 encoding`)
            writeFileSync(tmpCharFile, xml)
        }

        const scriptPath = join(localPath, '..', constants.SCRIPTS_DIR)

        const pythonHash = spawnSync('python3', [join(scriptPath, constants.HASH), tmpCharFile],
            {
                cwd: process.cwd(),
                env: constants,
                stdio: 'pipe',
                encoding: 'utf-8'
            }
        )

        const hash = pythonHash.output[1]

        const pythonProcess = spawnSync('python3', [join(scriptPath, constants.XML2JSON), tmpCharFile],
            {
                cwd: process.cwd(),
                env: constants,
                stdio: 'pipe',
                encoding: 'utf-8'
            }
        )

        const rawdata = pythonProcess.output[1]

        // we use this interaction.id as the id for the new character
        // since snowflakes are unique in Discord, this ensures
        // that there will be no other character with the same id
        const newChar = new Character(i.id)
        newChar.sheet = JSON.parse(rawdata)
        newChar.hash = hash.trim()

        // if a player other than the gamemaster loads a character file,
        // he automatically gains control over it
        if (!i.member.roles.cache.some(role => role.name === constants.ROLE_GM)) {
            newChar.control.push(i.user.id)
        }

        const gs = i.client.games.get(i.guild.id)
        if (typeof gs === 'undefined') {
            logError(`No gamestate found for guild ${i.guild.id}.`)
            return
        }

        // if this user is not already in the players list, include he/she
        if (!gs.players.get(i.user.id)) {
            const player = new Player(i.user.id)
            player.name = `${i.user.username}#${i.user.discriminator}`
            gs.players.set(i.user.id, player)
        }

        // if a player other than the gamemaster loads a character file,
        // he automatically gains full visibility over it
        if (!i.member.roles.cache.some(role => role.name === constants.ROLE_GM)) {
            newChar.visibility.full.push(i.user.id)
        }

        // this ensures that necessary values are
        // present in the character sheet,
        // substituting default values whenever necessary
        newChar.sanitize()

        // now we need to check whether this file has already been uploaded:
        // if both hash and character name are found in the collection
        // of characters already loaded, we refuse this sheet
        let collision = false

        for (const char of gs.characters.values()) {
            if (char.hash === newChar.hash && char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME === newChar.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME) {
                collision = true
                logDebug('Character file already loaded')
            }
        }
  
        if (collision) {
            i.reply({
                content: colorRed(`The sheet for ${newChar.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME} has already been loaded. No changes were made.`),
                ephemeral: true
            })
        } else {
            gs.characters.set(newChar.id, newChar)
            gs.writeState()
            i.client.games.set(i.guild.id, gs)
            logDebug(`Loaded sheet for ${newChar.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME}`)
            // now we show the new character in a message
            const m = i.client.games.get(i.guild.id).characters.get(newChar.id).show()
            m.embed.setColor(styles.private)
            i.reply({
                embeds: [m.embed],
                files: [m.attach],
                ephemeral: true
            })
        }
    }
}
