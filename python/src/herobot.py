import discord
import os

from xml.dom.minidom import parse, parseString

#from webserver import webserver
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')
PEDRO_ID = os.getenv('PEDRO_ID')

print('Herobot starting...')

class CustomClient(discord.Client):
    async def on_ready(self):
        print(f'{client.user} has connected to Discord.')
        for guild in client.guilds:
            if guild.id == GUILD:
                break

        print(
            f'{client.user} is connected to the following guild:\n'
            f'{guild.name}(id: {guild.id})'
        )

        members = '\n - '.join([member.name for member in guild.members])
        print(f'Guild Members:\n - {members}')

    async def on_message(self, message):
        # do not answer my own messages
        if message.author == client.user:
            return

        if ('pô pedro' in message.content.lower()) \
          or ('pô <@' + PEDRO_ID in message.content.lower()) \
          or (('pô ' in message.content.lower()) and message.author.id == int(PEDRO_ID)):
            response = 'Pô Pedro!'
            await message.channel.send(response)

        if ('!loadfiles' in message.content.lower()):
            document = parse("chars/Erle.hdc")
            print(document.childNodes)
            print(document.documentElement)
            print(document.documentElement.childNodes)

client = CustomClient()

client.run(TOKEN)
