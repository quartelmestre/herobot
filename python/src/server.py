from flask import Flask
server = Flask(__name__)

import ptvsd
ptvsd.enable_attach(address=('0.0.0.0', 5678))

from herobot import runbot

runbot()

@server.route("/")

def hello():
    return "Herobot server online."

if __name__ == "__main__":
    server.run(debug=True, host='0.0.0.0', port=5000)
