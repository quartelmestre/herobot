from flask import Flask
from threading import Thread

#webserver = Flask('')
webserver = Flask(__name__)

import ptvsd
ptvsd.enable_attach(address=('0.0.0.0', 5678))

@webserver.route('/')

def home():
    return "Herobot server online."

def run():
    if __name__ == "__main__":
        webserver.run(debug=True, host='0.0.0.0', port=5000)

#def run():
#    webserver.run(host='0.0.0.0',port=5000)

def webserver():
    t = Thread(target=run)
    t.start()
