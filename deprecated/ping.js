const { SlashCommandBuilder } = require('@discordjs/builders')
// const { bold, italic, strikethrough, underscore, spoiler, quote, blockQuote } = require('@discordjs/builders');
const { bold, italic } = require('@discordjs/builders')

module.exports = {
  data: new SlashCommandBuilder()
    .setName('ping')
    .setDescription('Replies with Pong!'),
  async execute (interaction, charList) {
    const timeTaken = Date.now() - interaction.createdTimestamp
    await interaction.reply(italic('Pong!') + ' This message had a latency of ' + timeTaken + bold('ms.'))
  }
}
