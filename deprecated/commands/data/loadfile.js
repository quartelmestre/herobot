'use strict'

import { SlashCommandBuilder } from '@discordjs/builders'

export default {
  data: new SlashCommandBuilder()
    .setName('loadfile')
    .setDescription('Loads a character HDC file.')
    .addAttachmentOption(
      option => option
        .setRequired(true)
        .setName('file')
        .setDescription('Attach the Hero Designer file.')
    )
}
