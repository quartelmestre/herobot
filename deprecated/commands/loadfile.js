'use strict'

import 'dotenv/config.js'

import { logDebug, logError } from '../library/_logger.js'

import { writeFileSync } from 'node:fs'

import { join } from 'node:path'

import { spawnSync } from 'child_process'

import { Character } from '../library/_character.js'

export default {
  async execute (interaction, gameState) {
    let answer = ''
    logDebug(`/loadfile command: user is ${interaction.user}`)

    const attachment = interaction.options.getAttachment('file')
    if (!attachment) {
      logError('No attached file found')
      return
    }

    const name = attachment.name
    const url = attachment.url
    // const proxyURL = attachment.proxyURL
    logDebug(`Attachment URL: ${url}`)
    logDebug(`Fetching data from file ${name}...`)

    let xml

    try {
      // fetch the file from the external URL
      const response = await fetch(url)

      // if there was an error send a message with the status
      if (!response.ok) {
        logError(`There was an error with fetching the file: ${response.statusText}`)
        return
      }

      logDebug(`File ${name} loaded`)
      // take the response stream and read it to completion
      xml = await response.text()
    } catch (error) {
      logError(error)
    }

    const tmpCharFile = join(process.env.HOME_DIR, process.env.TMP_CHAR_FILE)

    // FE FF: Big Endian
    // FF FE: Little Endian

    if ((Buffer.from(xml[0]).toString('hex') === 'efbfbd') && (Buffer.from(xml[1]).toString('hex') === 'efbfbd')) {
      logDebug('file is encoded as UTF-16BE, must be converted to UTF-16 LE')
      const stringBE = Buffer.from(xml)
      stringBE.swap16()
      stringBE[0] = 0xFF
      stringBE[1] = 0xFE

      logDebug(`Writing ${tmpCharFile} in UTF-16 LE encoding`)
      writeFileSync(tmpCharFile, stringBE.toString().substring(stringBE.toString().indexOf('<')))
    } else {
      logDebug('File is encoded as UTF-8 or ASCII, does not need conversion')
      logDebug(`Writing ${tmpCharFile} in UTF-8 encoding`)
      writeFileSync(tmpCharFile, xml)
    }

    const scriptPath = join(process.env.HOME_DIR, process.env.SCRIPTS_DIR, process.env.XML2JSON)

    const pythonProcess = spawnSync('python3', [scriptPath, tmpCharFile],
      {
        cwd: process.cwd(),
        env: process.env,
        stdio: 'pipe',
        encoding: 'utf-8'
      }
    )

    const rawdata = pythonProcess.output[1]
    const newChar = new Character(interaction.id)
    newChar.sheet = JSON.parse(rawdata)
    newChar.sheet.CHARACTER.IMAGE = {}

    gameState.characters.set(newChar.id, newChar)
    gameState.writeState()

    logDebug(`Loaded sheet for ${newChar.sheet.CHARACTER.CHARACTER_INFO['@CHARACTER_NAME']}`)

    answer += `I have just loaded ${newChar.sheet.CHARACTER.CHARACTER_INFO['@CHARACTER_NAME']}'s sheet.`

    await interaction.reply(answer)
  }
}
