'use strict'

import { logDebug } from '../library/_logger.js'

import { SlashCommandBuilder } from '@discordjs/builders'

export default {
  data: new SlashCommandBuilder()
    .setName('server')
    .setDescription('Displays information about the server.'),
  async execute (interaction, gameState) {
    const guild = interaction.guild
    const member = guild.members.cache.get(interaction.user.id)
    logDebug(`/server command: user is admin? ${member.permissions.has('ADMINISTRATOR')}`)
    let answer = `Server name: ${interaction.guild.name}`
    answer += `\nTotal members: ${interaction.guild.memberCount}`
    answer += `\nTotal characters loaded: ${gameState.characters.length}`
    if (member.permissions.has('ADMINISTRATOR')) {
      answer += '\nYou are authorized to use GM commands.'
    } else {
      answer += '\nYou are NOT authorized to use GM commands.'
    }

    await interaction.reply(answer)
  }
}
