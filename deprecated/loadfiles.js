'use strict'

import 'dotenv/config.js'

import { logInfo, logDebug } from '../library/_logger.js'

import { join } from 'node:path'
import { readdirSync } from 'node:fs'
import * as url from 'url'

import { spawnSync } from 'child_process'

import { Character } from '../library/_chars.js'

import { SlashCommandBuilder } from '@discordjs/builders'

// const __filename = url.fileURLToPath(import.meta.url)
const __dirname = url.fileURLToPath(new URL('.', import.meta.url))

export default {
  data: new SlashCommandBuilder()
    .setName('loadfiles')
    .setDescription('Loads character files.'),
  async execute (interaction, charList) {
    let answer = ''
    const guild = interaction.guild
    const member = guild.members.cache.get(interaction.user.id)
    if (!member.permissions.has('ADMINISTRATOR')) {
      answer += 'You are NOT authorized to use GM commands.'
      return
    }
    logDebug(`/loadfiles command: user is admin? ${member.permissions.has('ADMINISTRATOR')}`)
    charList.time = Date.now()
    charList.chars = []

    // Loads character files
    const charsPath = join(__dirname, '../chars')
    const scriptsPath = join(__dirname, '../scripts')
    const charFiles = readdirSync(charsPath).filter(file => file.endsWith('.hdc'))
    for (const file of charFiles) {
      const filePath = join(charsPath, file)
      const scriptPath = join(scriptsPath, 'xml2json.py')

      const pythonProcess = spawnSync('python3', [scriptPath, filePath],
        {
          cwd: process.cwd(),
          env: process.env,
          stdio: 'pipe',
          encoding: 'utf-8'
        }
      )

      const rawdata = pythonProcess.output[1]
      const loadedChar = JSON.parse(rawdata)
      const newChar = new Character(interaction.user.id, loadedChar)
      logDebug(`Loaded sheet for ${newChar.sheet.CHARACTER.CHARACTER_INFO['@CHARACTER_NAME']}`)
      charList.addChar(newChar)
    }
    const charListDate = new Date(charList.time)
    charList.writeList()
    logInfo(`Character list created at ${charListDate.toLocaleString('sv-SE', { timeZone: process.env.TZ })}`)
    answer = `${charList.chars.length} characters loaded`
    await interaction.reply(answer)
  }
}
