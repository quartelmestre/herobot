'use strict'

import { logDebug } from '../library/_logger.js'

import { SlashCommandBuilder } from '@discordjs/builders'

export default {
  data: new SlashCommandBuilder()
    .setName('testsimple')
    .setDescription('Used for testing simple commands (no attachments, no further options.'),
  async execute (interaction, charList) {
    logDebug(`/testsimple command: user is ${interaction.user}`)
    await interaction.reply(`Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}`)
  }
}
