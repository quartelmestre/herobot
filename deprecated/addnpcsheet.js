'use strict'

import 'dotenv/config.js'

import { logError, logDebug } from '../library/_logger.js'

import { Character } from '../library/_chars.js'

import { SlashCommandBuilder } from '@discordjs/builders'

const fetch = (...args) =>
  import('node-fetch').then(({ default: fetch }) => fetch(...args))

export default {
  data: new SlashCommandBuilder()
    .setName('addnpcsheet')
    .setDescription('Sends the .HDC file for a non-player character.')
    .addAttachmentOption((option) => option
      .setRequired(true)
      .setName('file')
      .setDescription('The NPC .hdc file')),
  async execute (interaction, charList) {
    let answer = ''

    const guild = interaction.guild
    const member = guild.members.cache.get(interaction.user.id)
    if (!member.permissions.has('ADMINISTRATOR')) {
      answer += 'You are NOT authorized to use GM commands.'
      return
    }

    const attachment = interaction.options.getAttachment('file')
    if (!attachment) {
      logError('No attached file found')
      return
    }

    const name = attachment.name
    const url = attachment.url
    // const proxyURL = attachment.proxyURL
    logDebug(`Attachment URL: ${url}`)
    logDebug(`Fetching data from file ${name}...`)

    let xml

    try {
      // fetch the file from the external URL
      const response = await fetch(url)

      // if there was an error send a message with the status
      if (!response.ok) {
        logError(`There was an error with fetching the file: ${response.statusText}`)
        return
      }

      logDebug(`File ${name} loaded`)
      // take the response stream and read it to completion
      xml = await response.text()
    } catch (error) {
      logError(error)
    }

    const substr = xml.substring(xml.indexOf('<'))
    console.log(substr)

    //    const options = {
    //      ignoreAttributes: false,
    //      allowBooleanAttributes: true,
    //      attributeNamePrefix: ''
    //    }

    // const parser = new XMLParser(options)
    const char = new Character()
    char.user = interaction.user.id

    // const json = parser.parse(xml)
    logDebug(`New NPC loaded: ${char.sheet.CHARACTER_INFO.CHARACTER_NAME}`)
    char.writeChar()
    charList.addChar(char)
    answer += `I have just loaded ${char.sheet.CHARACTER_INFO.CHARACTER_NAME}'s sheet.`

    await interaction.reply(answer)
  }
}
