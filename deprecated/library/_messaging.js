'use strict'

import 'dotenv/config.js'

// Messaging has classes and methods for the game to send information to the partipants

// sends a full-fledged message, possibly with several buttons, menus, attachments, etc.
export class FullMessage {
    constructor () {
        this.title = null
        this.content = null
        this.embeds = []
        this.avatar = null
        this.files = []
        this.author = null
        this.ephemeral = true
        this.components = []

    }

    // sends the message as an interaction reply
    async reply (interaction) {
        // the embeds author can be set by the calling function
        // this allows for messages from characters in the game
        // if not set, the author is the originating user
        if (this.embeds[0].author === null) {
            this.embeds[0].setAuthor({ name: interaction.user.username, iconURL: interaction.user.displayAvatarURL() })
        }
        await interaction.reply(
            {
                content: this.content,
                ephemeral: this.ephemeral,
                embeds: this.embeds,
                components: this.components,
                files: this.files,
                fetchReply: true
            }
        )
            // this function will only be activated when the interaction.reply
            // has been successfully completed
            // message is the local copy of the reply
            .then(message => {
                // we set a timeout
                // all buttons and menus are disabled after the timeout period
                // they may even have already been disabled by a button being clicked
                // (code in interactionCreate.js)
                setTimeout(async function () {
                    message.components.forEach(row => {
                        row.components.forEach(element => {
                            element.setDisabled(true)
                        })
                    })
                    // the reply to the original interaction is updated
                    // with the now-disabled rows
                    interaction.editReply({ components: message.components })
                }, process.env.TIMEOUT)
            })

    }

    // sends the message as an interaction follow-up
    async followUp (interaction) {
        if (this.embeds[0].author === null) {
            this.embeds[0].setAuthor({ name: interaction.user.username, iconURL: interaction.user.displayAvatarURL() })
        }
        await interaction.followUp(
            {
                content: this.content,
                ephemeral: this.ephemeral,
                embeds: this.embeds,
                components: this.components,
                files: this.files
            }
        )
            // this function will only be activated when the interaction.followUp
            // has been successfully completed
            // message is the local copy of the reply
            .then(message => {
                // we set a timeout
                // all buttons and menus are disabled after the timeout period
                // they may even have already been disabled by a button being clicked
                // (code in interactionCreate.js)
                setTimeout(() => message.delete(), process.env.TIMEOUT)
            })
    }

}

// sends a simple message, with no frills
export class SimpleMessage {
    constructor (content) {
        this.content = content
        // by default, the message is sent only to the user
        // this can be overriden in any instance
        this.ephemeral = true 
    }

    // sends the message as a reply
    async reply (interaction) {
        await interaction.reply(
            {
                content: this.content,
                ephemeral: this.ephemeral
            }
        )
    }

    // sends the message as a follow-up
    async followUp (interaction) {
        await interaction.followUp(
            {
                content: this.content,
                ephemeral: this.ephemeral
            }
        )
    }
}
