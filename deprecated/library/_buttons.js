'use strict'

import { logDebug } from '../library/_logger.js'

export function buttons (btnReceived, gameState) {
  let content = 'All right.'
  const button = btnReceived.split('.')

  switch (button[0]) {
    case 'btnPoPedro': {
      content = 'Pô Pedro!'
      break
    }
    case 'btnRemovePlayer': {
      const player = gameState.players.get(button[1])
      const name = player.name
      for (const char of gameState.characters.values()) {
        const index = char.control.indexOf(player.id)
        if (index > -1) {
          char.control.splice(index, 1)
        }
      }
      gameState.players.delete(player.id)
      gameState.writeState()
      content = `Player ${name} has been removed from the game.`
      logDebug(`Player ${name} removed from the game.`)
      break
    }
    case 'btnNotRemovePlayer': {
      const player = gameState.players.get(button[1])
      const name = player.name
      logDebug(`Player ${name} not removed from the game.`)
      content = `${name} has no idea he was almost kicked from the game.`
      break
    }
    case 'btnRemoveChar': {
      const char = gameState.characters.get(button[1])
      const name = char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME
      logDebug(`Character ${name} removed`)
      if (gameState.characters.delete(button[1])) {
        gameState.writeState()
        content = `${name} has been removed from the game.`
      } else {
        content = 'There has been an error: no character was removed.'
      }
      break
    }
    case 'btnNotRemoveChar': {
      const char = gameState.characters.get(button[1])
      const name = char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME
      logDebug(`Character ${name} not removed`)
      content = `${name} heaves a sigh of relief.`
      break
    }
    case 'btnAssociateChar': {
      const char = gameState.characters.get(button[1])
      const name = char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME
      const player = gameState.players.get(button[2])
      const modified = gameState.characters.get(button[1])
      modified.control.push(player.id)
      gameState.characters.set(button[1], modified)
      content = `Character ${name} was associated to ${player.name}.`
      logDebug(`Character ${name} associated to ${player.name}`)
      gameState.writeState()
      break
    }
    case 'btnNotAssociateChar': {
      const char = gameState.characters.get(button[1])
      const name = char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME
      logDebug(`Character ${name} not associated`)
      content = `${name} sighs, feeling forlorn.`
      break
    }
  }
  return content
}
