'use strict'

// buttonmanager has classes and methods for ready-made buttons
// the actual code run by each button is in the buttons collection

export class ButtonPoPedro {
    constructor () {
        this.code = 'btnPoPedro'
    }
}
