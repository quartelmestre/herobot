'use strict'

import 'dotenv/config.js'

import { SimpleMessage } from '../library/_messaging.js'

import { dateStringISO } from '../library/_date.js'

import { colorRed } from '../library/_styling.js'

export default {
    data: {
        name: 'btnLoadBackup',
        description: 'Loads the previous gamestate file.'
    },
    async execute (interaction) {
        await interaction.client.gameState.readBackup()
        await interaction.client.gameState.writeState()

        const message = new SimpleMessage(colorRed(`Game state from ${dateStringISO(interaction.client.gameState.time)} has been loaded.`))
        await interaction.followUp(message)

    }
}

