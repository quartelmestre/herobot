'use strict'

import 'dotenv/config.js'

import { FullMessage } from '../library/_messaging.js'

import { MessageEmbed } from 'discord.js'

import { dateStringISO } from '../library/_date.js'

import { colorGreen } from '../library/_styling.js'

export default {
    data: {
        name: 'btnListCharacters',
        description: 'Lists all visible characters.'
    },
    async execute (interaction) {
        
        let list = ''
        let count = 0

        for (const char of interaction.client.gameState.characters.values()) {
            if (char.visibility.full.includes(interaction.user.id) || char.visibility.partial.includes(interaction.user.id)) {
                list += char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME
                list += ` (loaded on ${dateStringISO(char.time)})`
                let controllers = ''
                char.control.forEach(element => {
                    if (element !== process.env.GAMEMASTER_ID) {
                        controllers += `${interaction.client.gameState.players.get(element).name}, `
                    }                        
                })
                controllers = controllers.slice(0, -2)
                if (controllers.length > 0) {
                    list += colorGreen(`controlled by ${controllers}`)
                }

                count++
            }
        }

        const message = new FullMessage()
        const embed = new MessageEmbed()
            .setColor('BLURPLE')
            .setTitle('List of characters')
            .setDescription(list)
            .setFooter({ text: `Total characters shown: ${count}` })

        message.embeds = [embed]
        await message.followUp(interaction)

    }
}

