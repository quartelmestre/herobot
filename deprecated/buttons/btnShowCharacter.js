'use strict'

import 'dotenv/config.js'

import { FullMessage } from '../library/_messaging.js'

import { MessageEmbed } from 'discord.js'

import { dateStringISO } from '../library/_date.js'

import { colorGreen } from '../library/_styling.js'

export default {
    data: {
        name: 'btnShowCharacter',
        description: 'Show detailed information on the selected character.'
    },
    async execute (interaction) {
        console.log(interaction.user)
        console.log(interaction.client.gameState.players.get(interaction.user.id))
        const id = interaction.client.gameState.players.get(interaction.user.id).active.character
        const char = interaction.client.gameState.characters.get(id)
        await char.show()
        console.log('cheguei')
    }
}

