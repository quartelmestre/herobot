'use strict'

import 'dotenv/config.js'

import { FullMessage } from '../library/_messaging.js'

import { PoPedro } from '../library/_styling.js'

import { MessageAttachment, MessageEmbed } from 'discord.js'

export default {
    data: {
        name: 'btnPoPedro',
        description: 'Button to send a "Pô Pedro" message.'
    },
    async execute (interaction) {
        
        const message = new FullMessage()
        const embed = new MessageEmbed()
            .setColor('RED')
            .setImage(`attachment://${PoPedro.filename}`)
        const buffer = Buffer.from(PoPedro.image, 'base64')
        const attach = new MessageAttachment(buffer, PoPedro.filename)
        message.files = [attach]
        message.ephemeral = false
        message.embeds = [embed]
        await message.followUp(interaction)

    }
}

