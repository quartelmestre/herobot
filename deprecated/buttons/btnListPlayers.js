'use strict'

import 'dotenv/config.js'

import { FullMessage } from '../library/_messaging.js'

import { MessageEmbed } from 'discord.js'

import { dateStringISO } from '../library/_date.js'

import { colorGreen, colorRed, colorYellow } from '../library/_styling.js'

export default {
    data: {
        name: 'btnListPlayers',
        description: 'Lists all players.'
    },
    async execute (interaction) {
        
        let list = ''

        for (const player of interaction.client.gameState.players.values()) {
            list += `${player.name}`
            list += ` (since ${dateStringISO(player.time)})`
            if (player.id === process.env.GAMEMASTER_ID) {
                list += colorRed('Gamemaster')
            } else {
                let controlledChars = ''
                for (const char of interaction.client.gameState.characters.values()) {
                    if(char.control.includes(player.id)) {
                        controlledChars += `${char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME}, `
                    }
                }
                controlledChars = controlledChars.slice(0, -2)
                if (controlledChars.length > 0) {
                    list += colorGreen(`controls ${controlledChars}`)
                } else {
                    list += colorYellow('no controlled characters')
                }
            }
        }

        const message = new FullMessage()
        const embed = new MessageEmbed()
            .setColor('BLURPLE')
            .setTitle('List of players')
            .setDescription(list)
            .setFooter({ text: `Total players in game: ${interaction.client.gameState.players.size}` })

        message.embeds = [embed]
        await message.followUp(interaction)

    }
}

