'use strict'

import 'dotenv/config.js'

import { FullMessage } from '../library/_messaging.js'

import { MessageEmbed, MessageActionRow, MessageSelectMenu } from 'discord.js'

import { colorRed } from '../library/_styling.js'

import { dateStringISO } from '../library/_date.js'

export default {
    data: {
        name: 'btnRemoveCharacter',
        description: 'Removes a character from the game.'
    },
    async execute (interaction) {

        const row = new MessageActionRow()
        const menu = new MessageSelectMenu()
            .setCustomId('menuCharToBeRemoved')
        
        for (const char of interaction.client.gameState.characters.values()) {
            if (char.control.includes(interaction.user.id)) {
                menu.addOptions({
                    label: char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME,
                    value: char.id,
                    description: dateStringISO(char.time)
                })
            }
        }

        const message = new FullMessage()
        const embed = new MessageEmbed()
            .setColor('RED')
            .setTitle('Remove a character')

        if (menu.options.length > 0) {
            row.addComponents(menu)
            message.components = [row]
            embed.setDescription('Please select a character to be removed:')
        } else {
            embed.setDescription(colorRed('You do not control any character.'))
        }

        message.embeds = [embed]
        message.ephemeral = false
        
        await message.followUp(interaction)

    }
}

