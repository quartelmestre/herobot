const winston = require('winston')
 
const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.errors({ stack: true }),
        winston.format.json()
    ),
    transports: [
        new winston.transports.File(
            {
                filename: 'log/error.json', 
                level: 'error'
            }
        ),
        new winston.transports.File(
            {
                filename: 'log/debug.json',
                level: 'debug'
            }
        ),
        new winston.transports.File(
            {
                filename: 'log/info.json',
                level: 'info'
            }
        )
    ],
})
 
if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }))
}
 
module.exports = logger
