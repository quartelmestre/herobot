# Program to convert an xml
# file to json file

# import json module and xmltodict
# module provided by python
import json
import xmltodict
import sys
# import os
import urllib.request, urllib.parse, urllib.error

url = sys.argv[1]

hdc = urllib.request.urlopen(url)
local = open('local.xml', 'wb')
size = 0
while True:
    info = hdc.read(100000)
    if len(info) < 1: break
    size = size + len(info)
    local.write(info)

# print(size, 'characters copied.')
local.close()

# open the input xml file and read
# data in form of python dictionary
# using xmltodict module
with open('local.xml', 'rb') as xml_file:
	
	data_dict = xmltodict.parse(xml_file.read())
	xml_file.close()
	
	print(json.dumps(data_dict))

	# generate the object using json.dumps()
	# corresponding to json data
	#json_data = json.dumps(data_dict)
	
	# Write the json data to output
	# json file
	#with open("Erle.json", "w") as json_file:
	#	json_file.write(json_data)
	#	json_file.close()

# os.remove('local.xml')
