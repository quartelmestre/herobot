'use strict'

// const logger = require('../library/_logger')
import { writeFileSync } from 'node:fs'
import { join } from 'node:path'
import * as url from 'url'

const __dirname = url.fileURLToPath(new URL('.', import.meta.url))
const charsPath = join(__dirname, '../chars')

// creates a hash which can be used as an id for any character
// source: https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript

const cyrb53 = function (str, seed = 0) {
  let h1 = 0xdeadbeef ^ seed
  let h2 = 0x41c6ce57 ^ seed
  for (let i = 0, ch; i < str.length; i++) {
    ch = str.charCodeAt(i)
    h1 = Math.imul(h1 ^ ch, 2654435761)
    h2 = Math.imul(h2 ^ ch, 1597334677)
  }
  h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909)
  h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909)
  return 4294967296 * (2097151 & h2) + (h1 >>> 0)
}

class Character {
  constructor (user, hdc) {
    this.time = Date.now()
    this.user = user
    this.visibility = {
      full: [],
      partial: []
    }
    this.sheet = hdc
  }

  writeChar () {
    const filePath = join(charsPath, (this.sheet.CHARACTER_INFO.CHARACTER_NAME + '.json'))
    const data = JSON.stringify(this, null, 2)
    writeFileSync(filePath, data)
  }

  createID (string) {
    return cyrb53(string)
  }
}

class CharList {
  constructor () {
    this.time = Date.now()
    this.chars = []
  }

  addChar (char) {
    this.chars.push(char)
  }

  writeList () {
    const filePath = join(charsPath, 'charList')
    const data = JSON.stringify(this, null, 2)
    writeFileSync(filePath, data)
  }
}

const _Character = Character
export { _Character as Character }
const _CharList = CharList
export { _CharList as CharList }
