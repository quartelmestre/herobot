'use strict'

const logger = require('../library/_logger')

const { SlashCommandBuilder } = require('@discordjs/builders')

module.exports = {
  data: new SlashCommandBuilder()
    .setName('user')
    .setDescription('Displays information about the user.'),
  async execute (interaction, charList) {
    logDebug(`/user command: user is ${interaction.user}`)
    await interaction.reply(`Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}`)
  }
}
