'use strict'

import 'dotenv/config.js'

import { logDebug, logError } from '../library/_logger.js'

import { SimpleMessage } from '../library/_messaging.js'

import { colorRed } from '../library/_styling.js'

export default {
    data: {
        name: 'menuCharToBeRemoved',
        description: 'Removes a character from the game.'
    },
    async execute (interaction) {

        const char = interaction.client.gameState.characters.get(interaction.values[0])
        interaction.client.gameState.players.forEach(element => {
            if (element.active.character === char.id) {
                element.active.char = null
                element.active.channel = null
            }
        })

        let removeMessage
        try {
            interaction.client.gameState.characters.delete(interaction.values[0])
            interaction.client.gameState.writeState()
            logDebug(`Character ${char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME} has been removed.`)
            removeMessage = new SimpleMessage(colorRed(`Character ${char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME} has been removed.`))
        } catch (error) {
            logError(error)
            removeMessage = new SimpleMessage(colorRed(`ERROR: character ${char.sheet.CHARACTER.CHARACTER_INFO.CHARACTER_NAME} has NOT been removed.`))
        }
        
        await removeMessage.followUp(interaction)
    }
}

