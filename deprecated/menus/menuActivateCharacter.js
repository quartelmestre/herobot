'use strict'

import 'dotenv/config.js'

export default {
    data: {
        name: 'menuActivateCharacter',
        description: 'Activates a character for subsequent commands.'
    },
    execute (interaction) {
        const char = interaction.client.gameState.characters.get(interaction.values[0])
        interaction.client.gameState.players.get(interaction.user.id).active.character = char.id
        interaction.deferReply()
            .then(console.log('then'))
            .catch(console.log('catch'))
    }
}

