# Miscellaneous notes

## Commands

### /loadgame

This is a guild admin only command. It loads a game file, or starts a new game in this server.

### /loadfile

Loads a Hero Designer game file

### /start

This is a GM only command, to be used at the start of a session.

It activates the player characters and creates the player threads.

### /info

Shows a list of visible characters

### /target

Allows to select targets for a future action

### /emote and /talk

Sends a message as the character

### /commonroll

GM only: makes a roll for all target PCs (usually PER)

### /roll

### /secretroll

### /check

## Data structures

### Interactions

interaction
  .id                 snowflake daquela interação em especial
  .applicationId      snowflake do bot
  .commandId          snowflake do comando em execução
  .commandName        nome do comando em execução
  .channelId          snowflake do canal
  .guildId            snowflake do servidor (guilda)
  .user.id            snowflake de quem ativou a interação
       .username
       .discriminator
  .member.guild.nickname
               .user
