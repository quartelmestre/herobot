# Hero bot

Discord bot for Hero System v6 RPG rules.

## Folder structure

- src: JS source files
  - commands: JS source files for slash commands
  - events: JS source files for client events
  - game: JSON game state files
  - library: classes and auxiliary functions
  - scripts: non-JS external scripts
