# specify the node base image with your desired version node:<version>
FROM node:current
RUN groupmod -g 1000 node && usermod -u 1000 -g 1000 node
# replace this with your application's default port
EXPOSE 30000

# Create the bot's directory
#RUN mkdir -p /usr/src/bot
WORKDIR /home/node/app

COPY /src/ ./

RUN apt-get update

# python is used in the script to read Hero Designer files
RUN apt-get install --yes python3 python3-pip
RUN pip install --break-system-packages xmltodict

# installation of javascript modules
RUN npm install

# automatic restart when source doce changes
RUN npm install nodemon

# logger module
RUN npm install winston

# loads environment variables from .env file into proccess.env
RUN npm install dotenv

# enables fetching a file from a URL
RUN npm install node-fetch

# discord-specific modules
RUN npm install @discordjs/builders @discordjs/rest discord-api-types
RUN npm install @discordjs/rest discord-api-types
RUN npm install slash-commands
RUN npm install discord.js

# Start the bot.
#CMD ["node", "index.js"]
